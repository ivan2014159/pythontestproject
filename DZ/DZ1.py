
def validate_date(day, month, year):
    # Проверка на месяц и день
    if month < 1 or month > 12 or day < 1: 
        return False

    # Список дней в месяцах
    days_in_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    # Проверк на високосный год
    if (year % 4 == 0 and year % 100 != 0) or year % 400 == 0:
        days_in_month[1] = 29

    if day > days_in_month[month - 1]:
        return False

    return True


def zad1(day, month, year):
    """Задание 1"""
    if (validate_date(day, month, year)):
        month_names = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 
                       'июля', 'августа', 'сентября', 'октября', 'ноября', 
                       'декабря']
        date = f'{day} {month_names[month - 1]} {year} года'  
        return date
    else:
        return 'Некорректная дата'

print(zad1(19, 11, 2023))

def zad2(names):
    """Задание 2"""
    name_count = {}
    for name in names:
        name_count[name] = name_count.get(name, 0) + 1
    return name_count

print(zad2(('Олег', 'Игорь', 'Анна', 'Анна', 'Игорь', 'Анна', 'Василий')))


def zad3(person_data):
    """Задание 3"""
    first_name = person_data.get('first_name', '')
    last_name = person_data.get('last_name', '')
    middle_name = person_data.get('middle_name', '')

    if ((not first_name and not last_name and not middle_name)
        or (not first_name and not last_name and middle_name)):
        return 'Нет данных'
    elif not last_name:
        return f'{first_name} {middle_name}'
    elif not middle_name:
        return f'{last_name} {first_name}'
    elif not first_name:
        return f'{last_name}'
    else:
        return f'{last_name} {first_name} {middle_name}'

print(zad3({'first_name': 'Иван', 'last_name': 'Иванов', 
            'middle_name': 'Иванович'}))

def zad4(number):
    """Задание 4"""
    if number < 2:
        return False
    for i in range(2, int(number**0.5) + 1):
        if number % i == 0:
            return False
    return True

print(zad4(11))

def zad5(*args):
    """Задание 5"""
    unique_nums = []
    for arg in args:
        if isinstance(arg, (int, float)) and arg not in unique_nums:
            unique_nums.append(arg)

    return sorted(unique_nums)

print(zad5(1, '2', 'text', 42, None, None, None, 15, True, 1, 1))