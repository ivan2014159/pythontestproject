class Counter:
    def __init__(self, initial_value):
        self._value = initial_value

    def inc(self):
        self._value += 1
        return self._value

    def dec(self):
        self._value -= 1
        return self._value
    
class ReverseCounter(Counter):
    def inc(self):
        self._value -= 1
        return self._value

    def dec(self):
        self._value += 1
        return self._value
    
def get_counter(number):
    return Counter(number) if (number >= 0) else ReverseCounter(number)


counter = get_counter(5)
a = counter.inc()
b = counter.inc()
c = counter.inc()
d = counter.dec()

print("При counter = get_counter(5):", a, b, c, d)

counter = get_counter(-1)
a = counter.inc()
b = counter.inc()
c = counter.inc()
d = counter.dec()

print("При counter = get_counter(-1):", a, b, c, d)
