from django.shortcuts import render
from django.views import View


class Homepage(View):
    def get(self, request):
        context = {'title':"Главная страница" ,'text': "Пока ещё не придумал тему, пока будет пустая страница"}
        return render(request=request, template_name='core\mainpage.html', context=context)
    
class Info(View):
    def get(self, request):
        context = {'title':"Информаиция о проекте" ,'text': "Тут тоже пока пусто, но надеюсь скоро что-нибудь появится"}
        return render(request=request, template_name='core\info.html', context=context)